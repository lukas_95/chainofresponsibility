<?php

namespace NineBits\Chain\Service;

use NineBits\Chain\Model\User;

class FirstNameValidator extends AbstractUserValidator
{

    public function validate(User $user): string
    {
        if($user->getFirstName() == 'Tytus')
        {
            return $this->getNext()->validate($user);
        }

        return 'Imię jest niepoprawne';
    }
}
