<?php

namespace NineBits\Chain\Service;

use NineBits\Chain\Model\User;

abstract class AbstractUserValidator
{
    private AbstractUserValidator $next;

    abstract public function validate(User $user): string;

    public function getNext()
    {
        if (!isset($this->next)) {
            return new DefaultUserValidator();
        }

        return $this->next;
    }

    public function setNext(AbstractUserValidator $next): AbstractUserValidator
    {
        $this->next = $next;
        return $next;
    }
}
