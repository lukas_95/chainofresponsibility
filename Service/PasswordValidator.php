<?php

namespace NineBits\Chain\Service;

use NineBits\Chain\Model\User;

class PasswordValidator extends AbstractUserValidator
{

    public function validate(User $user): string
    {
        if($user->getPassword() == 'kapitan')
        {
            return $this->getNext()->validate($user);
        }

        return 'Hasło jest niepoprawne';
    }
}
