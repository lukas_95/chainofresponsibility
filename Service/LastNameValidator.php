<?php

namespace NineBits\Chain\Service;

use NineBits\Chain\Model\User;

class LastNameValidator extends AbstractUserValidator
{

    public function validate(User $user): string
    {
        if($user->getLastName() == 'Bomba')
        {
            return $this->getNext()->validate($user);
        }

        return 'Nazwisko jest niepoprawne';
    }
}
