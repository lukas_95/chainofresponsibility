<?php

namespace NineBits\Chain\Service;

use NineBits\Chain\Model\User;

class UserValidatorService
{
    private FirstNameValidator $firstNameValidator;
    private LastNameValidator $lastNameValidator;
    private PasswordValidator $passwordValidator;

    public function __construct(
        FirstNameValidator $firstNameValidator,
        LastNameValidator $lastNameValidator,
        PasswordValidator $passwordValidator
    )
    {
        $this->firstNameValidator = $firstNameValidator;
        $this->lastNameValidator = $lastNameValidator;
        $this->passwordValidator = $passwordValidator;
    }

    public function validateUser(User $user): string
    {
        $this->firstNameValidator->setNext($this->lastNameValidator);
        $this->lastNameValidator->setNext($this->passwordValidator);

        return $this->firstNameValidator->validate($user);
    }
}
