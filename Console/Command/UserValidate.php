<?php

namespace NineBits\Chain\Console\Command;

use NineBits\Chain\Model\User;
use NineBits\Chain\Service\UserValidatorService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UserValidate extends Command
{
    private UserValidatorService $userValidatorService;

    public function __construct(UserValidatorService $userValidatorService, string $name = null)
    {
        $this->userValidatorService = $userValidatorService;
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('user:validate');
        $this->setDescription('User Validate');
        $this->addOption(
            'first_name',
            null,
            InputOption::VALUE_REQUIRED,
            'First Name'
        );
        $this->addOption(
            'last_name',
            null,
            InputOption::VALUE_REQUIRED,
            'Last Name'
        );
        $this->addOption(
            'password',
            null,
            InputOption::VALUE_REQUIRED,
            'Password'
        );
        parent::configure();
    }

    /**
     * CLI command description
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return string
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = new User($input->getOption('first_name'),$input->getOption('last_name'), $input->getOption('password'));

        return $output->writeln($this->userValidatorService->validateUser($user));
    }
}
